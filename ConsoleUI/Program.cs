﻿using System;

namespace ConsoleUI
{
    public class Program
    {
        private const decimal EURO_RATE = 7.45M;
        private static string chosenCurrency;

        private static void Main()
        {
            /*
                Opgave 2.1
                Lav et program som omregner mellem danske kroner og Euro. Antag at kursen altid er 7.45
             */

            while (true)
            {
                Console.Clear();
                Console.WriteLine("*** Omregning mellem kroner og euro ***");
                ChooseCurrency();
                ReadAmount();
            }
        }

        private static void ChooseCurrency()
        {
            do
            {
                Console.WriteLine("Valutavalg: Skriv 'DK' for Kroner eller 'EU' for Euro:\n");
                chosenCurrency = Console.ReadLine().ToUpper();
            } while (chosenCurrency != "DK" && chosenCurrency != "EU");
        }

        private static void ReadAmount()
        {   
            Console.WriteLine("Indtast det ønskede beløb:\n");
            string input = Console.ReadLine();
            
            if (decimal.TryParse(input, out decimal amountRead))
            {
                CalculateAmount(amountRead);
            }            
            else
            {
                Console.WriteLine("Input skal være et tal.\n");
                Console.WriteLine("Tryk en vilkårlig tast for at prøve igen.");
                Console.ReadKey();
            }
        }

        private static void CalculateAmount(decimal amountRead)
        {
            decimal calculatedCurrency;

            if (chosenCurrency == "DK")
            {
                calculatedCurrency = amountRead / EURO_RATE;                
            }
            else
            {
                calculatedCurrency = amountRead * EURO_RATE;
            };

            string modsatValuta = (chosenCurrency == "EU") ? "DK" : "EU";

            Console.WriteLine($"\n\n{ amountRead.ToString("F2") } { chosenCurrency } svarer til { calculatedCurrency.ToString("F2") } { modsatValuta } ");
            
            Console.WriteLine("Tryk en vilkårlig tast for at lave en ny omregning.\n");
            Console.ReadKey();
        }
    }
}
